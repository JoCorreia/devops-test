FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

VOLUME //LIBS/:/usr/local/tomcat/webapps/

EXPOSE 8080
