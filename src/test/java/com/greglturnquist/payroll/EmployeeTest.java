package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Employee constructor - Happy path")
    public void constructorEmployee_happyPath() {
        //Arrange
        String firstName = "Jane";
        String lastName = "Doe";
        String description = "Some description";
        String jobTitle = "Developer";
        String email = "jane@outlook.com";

        String expectedToString = "Employee{" +
                "id=" + null +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", email='" + email + '\'' +
                '}';
        ;

        //Act
        Employee jane = new Employee(firstName, lastName, description, jobTitle, email);

        //Assert
        assertEquals(expectedToString, jane.toString());
    }

    @Test
    @DisplayName("Employee constructor - Exception - First Name")

    public void constructorEmployee_Exception_firstName() {

        //Arrange
        String firstName = null;
        String lastName = "Doe";
        String description = "Some description";
        String jobTitle = "Developer";
        String email = "jane@outlook.com";

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter firstName doesn't have a valid argument, hence Employee will not be created", thrown.getMessage());
    }

    @Test
    @DisplayName("Employee constructor - Exception - Last Name")

    public void constructorEmployee_Exception_lastName() {

        //Arrange
        String firstName = "Jane";
        String lastName = null;
        String description = "Some description";
        String jobTitle = "Developer";
        String email = "jane@outlook.com";

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter lastName doesn't have a valid argument, hence Employee will not be created", thrown.getMessage());
    }

    @Test
    @DisplayName("Employee constructor - Exception - Description")

    public void constructorEmployee_Exception_Description() {

        //Arrange
        String firstName = "Jane";
        String lastName = "Doe";
        String description = null;
        String jobTitle = "Developer";
        String email = "jane@outlook.com";

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter description doesn't have a valid argument, hence Employee will not be created", thrown.getMessage());
    }

    @Test
    @DisplayName("Employee constructor - Exception - Job title")

    public void constructorEmployee_Exception_jobTitle() {

        //Arrange
        String firstName = "Jane";
        String lastName = "Doe";
        String description = "Some description";
        String jobTitle = null;
        String email = "jane@outlook.com";

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter jobTitle doesn't have a valid argument, hence Employee will not be created", thrown.getMessage());
    }

    @Test
    @DisplayName("Employee constructor - Exception - Email")

    public void constructorEmployee_Exception_email() {

        //Arrange
        String firstName = "Jane";
        String lastName = "Doe";
        String description = "Some description";
        String jobTitle = "Developer";
        String email = null;

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter email doesn't have a valid argument, hence Employee will not be created", thrown.getMessage());
    }

    @Test
    @DisplayName("Employee constructor - Exception - Email must have the sign")

    public void constructorEmployee_emailMustHaveAt() {

        //Arrange
        String firstName = "Jane";
        String lastName = "Doe";
        String description = "Some description";
        String jobTitle = "Developer";
        String email = "jane.com";

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));

        //Assert
        assertEquals("The parameter email doesn't have @ sign, hence Employee will not be created", thrown.getMessage());
    }

}