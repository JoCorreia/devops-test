# README #

This repository holds the project files required to undertake the work expected for **Class Assignment 3, Part 2**, developed under the curricular unit 'DevOps' (SWitCH, 2019/2020).

The source code here contained is parte of the application from the tutorial _React.js and Spring Data REST_, available at https://github.com/spring-guides/tut-react-and-spring-data-rest. The application's module _'basic'_ has been converted to use Gradle instead of Maven (ca2.2).