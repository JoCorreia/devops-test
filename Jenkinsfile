pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://JoCorreia@bitbucket.org/JoCorreia/devops-test.git'
            }
        }
        stage('Assemble') {
            steps {           
                echo 'Assembling...'
                sh './gradlew clean assemble'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                sh './gradlew test'
                junit '**/build/test-results/test/*.xml'
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating Javadoc...'
                sh './gradlew javadoc'
                publishHTML (target: [allowMissing: false,
                    alwaysLinkToLastBuild: true,
                    keepAll: true,
                    reportDir: './',
                    reportFiles: 'index.html',
                    reportName: 'Javadoc'])
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/libs/*'
            }
        }
        stage('Publish image') {
            steps{
                echo 'Building and deploying image...'
                script {
                    def dockerImage = docker.build("jocorreia/devops-ca5-2-jenkins:${env.BUILD_ID}")
                    docker.withRegistry('https://registry.hub.docker.com', 'jocorreia-docker-hub') {
                        dockerImage.push()
                    }
                }
            }
        }
    }
}